$(function() {

  var header = $("#header"),
      mainH = $("#main").innerHeight(),
      scrollOffset = $(window).scrollTop();


  /* Fixed Header */
  checkScroll(scrollOffset);

  $(window).on("scroll", function() {
    scrollOffset = $(this).scrollTop();

    checkScroll(scrollOffset);
  });

  function checkScroll(scrollOffset) {
    if( scrollOffset >= mainH/2) {
      header.addClass("fixed");
    } else {
      header.removeClass("fixed");
    }
  }



  /* Smooth scroll */
  $("[data-scroll]").on("click", function(event) {
    event.preventDefault();

    var $this = $(this),
        blockId = $this.data('scroll'),
        blockOffset = $(blockId).offset().top;

    $("html, body").animate({
      scrollTop:  blockOffset
    }, 500);
  });

});

