﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace selfHostedHttp
{
	class Program
	{
		static void Main(string[] args)
		{
            HttpListener server = new HttpListener();
            server.Prefixes.Add("http://localhost:8080/");
            server.Start();
            Console.WriteLine("Сервер начал прослушивание порта 8080");
            while (true)
            {
                HttpListenerContext context = server.GetContext();
                HttpListenerRequest request = context.Request;
                HttpListenerResponse response = context.Response;
                var req = request.Url.ToString();
                var regex1 = new Regex(@"[\w]+\.(html|css|js|jpg|png|ico)");
                var regex2 = new Regex(@"html|css|js|img");
                var regex3 = new Regex(@"about|contacts|instagram|why_itis");
                var regex4 = new Regex(@"png|jpg");

                var file = regex1.Matches(req).Select(a => a.Value).FirstOrDefault();
                var ifImage = regex4.Matches(file).Select(a => a.Value).FirstOrDefault();
                var fold = regex2.Matches(req).Select(a => a.Value).FirstOrDefault() + @"/";
                var fold_img = regex3.Matches(req).Select(a => a.Value).FirstOrDefault() + @"/";
                if (fold_img.Length == 1 || ifImage == null)
                    fold_img = "";
                fold = fold.Length == 1 ? "" : fold;
                var bytes = File.ReadAllBytes(@"../../../../www/" + fold + fold_img + file);

                response.ContentLength64 = bytes.Length;

                Stream sw = response.OutputStream;
                sw.Write(bytes, 0, bytes.Length);
                sw.Close();
            }
        }
    }
}
